---
title: Hardergrat ridge
image: /images/2022-07-02-hardergrat-ridge-01.jpg
imageMeta:
  attribution:
  attributionLink:
featured: true
authors:
  - michal-bryxi
date: Thu Jul 21 2022 07:54:32 GMT+0200 (Central European Summer Time)
tags:
  - 5-stars
  - schweiz
---

If you Google the "ridge hike in Switzerland",
you're going to find,
in the top 10,
maybe even top 3
Brienzergrat also known as Hardergrat ridge.
Internet content starts with amazing drone videos,
through people being scared for their life and bailing off,
to people claiming that it's overrated.
So how was it from my perspective?

## Planning

For the first go
it seemed like a good idea to do the "easy" variant
going between the mountain tops:
[Harder Kulm](https://en.mapy.cz/s/nasaferonu) and [Brienzer Rothorn](https://en.mapy.cz/s/nalasaroso)
Since this is not a circular route,
we had to first decide which way to go.
We went with **Rothorn -> Harder** direction because:

1. It's "downhill" (1391m vs 2333m).
1. Last train from Rothorn [goes at 17:40](https://brienz-rothorn-bahn.ch/timetable-prices/?lang=en#1489570541006-a987678e-2759) whereas the funicular from Harder it's [going till 21:40](https://www.jungfrau.ch/de-ch/planen-buchen/fahrplan/).
1. Even if we missed the last funicular from Harder, we would finish in Interlaken and could easily walk home.
1. [The crux](https://en.mapy.cz/s/bopumelesu) is closer to Rothorn, so we encountered it still fresh.
1. It's East-West direction, so no sunrise glares.
1. There is a special [08:36 a.m. ticket](https://brienz-rothorn-bahn.ch/timetable-prices/?lang=en#1489570541006-a987678e-2759) at Brienz Rothorn Bahn that costs only _CHF 24.50_.

## Execution

Early start.
Coffee.
Pack running vest.
Properly tie shoelaces.
Go to [Interlaken Ost](https://en.mapy.cz/s/gotabufozu).
Board the train.
Sleep.
But not too much.
Change in Brienz.
Admire the [cutest train logo ever](https://brienz-rothorn-bahn.ch/timetable-prices/?lang=en).
Sleep for an hour, while counting the [six tunnels of BRB](https://de.wikipedia.org/wiki/Brienz-Rothorn-Bahn).
We're at the start of the trail!

The trail starts with an amazing set of views
that is completely worth it on their own
and got even better as we hiked on.

The first few kilometres were on a gentle ridge
that even had steps where necessary.
But despite the plan to run the trail,
we were mostly only walking.
Partially because of the downhill, technical terrain.
Partially because of the steep sides
that guaranteed a long tumble in case of a wrong step.
Reasonable, safe running can happen in the last 1/4 of the ridge.

Exactly **one-hour** after the start we met the **crux** of the route: The **Tannhorn**. Which is [described as](https://www.brienzergrat.com/brienzergrat_to_hardergrat.html):

> It’s the tallest peak along the route and to get up there requires a rocky climb up a short T5-rated outcrop of rock. 
> A portion of that climb does have a wire rope which is less helpful for holding onto,
> more for indicating the safe place to climb.
> It isn’t that difficult to climb,
> but it is very steep.
> The rest of the climb to the peak of Tannhorn along its long backbone is the most exposed and narrow section of the trail.

Speaking from personal experience from [some UK scrambles](/2021-06-07-an-teallach),
I doubt that in Scottish guidebooks
this would be marked as "exposed"
and even less so "with few bold moves".
It is not for the faint-hearted,
but good shoes,
slow moves
and three points of contact rule should anyone reasonably fit get through.

![Me on Tannhorn](/images/2022-07-02-hardergrat-ridge-03.jpg)

Speaking about the relativity of the perception of the dangers:
We've met quite a few people that just run the ridge as they'd be on tarmac.
And also a few that were genuinely scared for their lives when the ridge got narrow.
So it depends 🤷‍♂️.

![Hardergrat view towards Brienz](/images/2022-07-02-hardergrat-ridge-02.jpg)

Moving along the grassy tops we got an amazing set of views,
which can be only described as:
What you see on Instagram looks way worse than reality.


![Obligatory selfie](/images/2022-07-02-hardergrat-ridge-05.jpg)

When we passed 3/4 of the route,
the terrain became runnable and we could enjoy some "wheee" and "uiii".
As a bonus the path occasionally snaked into the forest,
so we could get a tiny break from the omnipresent sun.

![Finally some running opportunities](/images/2022-07-02-hardergrat-ridge-04.jpg)

Beer at the Harder Kulm kiosk
and late evening funicular down back to Interlaken.

## Wrap-up

I enjoyed this wee adventure very much.
The views,
the trail,
the tiny scrambles,
all of it.
Even talked to some locals that are hiking it every year
and they assured us that "this was the crux and it's going only easier from now on".
If you're in the area and have a day to kill,
then you should try it.

## Conclusion

- Make sure you can make the whole **20km** with **~ 1.1km** ascent and **~2km** descent before stepping a foot on this trail. While escape routes are possible, I'd highly recommend not taking them as some seem worse than the ridge itself.
- Start **early**. It took us **8 hours** to finish and we were _not_ slow.
- The clickbaity videos on YouTube from people bailing off the trail are often shot on a fish-eye lens and are over-dramatized for more clicks. There is certainly a danger, but a responsible hiker should not have a problem.
- I'd advise against doing the ridge when wet. The rocks on the path could get sketchy and you _really_ don't want to fall.
- Most important of them all and I can't stress this enough: Take enough water. There is **no** option for a refill and there is _next to zero_ shade on the way. We had **2.5l** per person and finished with a tiny reserve left.
- If you want to run-run this I think it would be better to do it the other way around. Because you're going to have fewer beaking-for-your-life moments.

## Stats

| Name        |      Value    |
|-------------|--------------:|
| Distance    |       20.15km |
| Ascent      |         1080m |
| Descent     |         2013m |
| Duration    |      8h 03min |
| Moving time |      4h 00min |

![Strava of Hardergrat trail run top to top](/images/2022-07-02-hardergrat-ridge-06.png)

- [Strava recording of Hardergrat trail run top to top](https://www.strava.com/activities/7403876714)
- [Brienzergrat FKT](https://fastestknowntime.com/route/brienzergrat-switzerland) (5h 42m 34s) 😱
