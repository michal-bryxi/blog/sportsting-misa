---
title: Jabra Elite Active 65t
image: /images/2020-04-12-jabra-elita-active-65t.jpg
imageMeta:
  attribution:
  attributionLink:
featured: true
authors: 
  - michal-bryxi
date: Sun Apr 12 2020 17:42:50 GMT+0100 (British Summer Time)
tags:
  - review
---

Video review of _wireless_ sports headphones from _Jabra_. 2 ~months of daily and very intense usage in various use-cases:
- running
- cycling
- video conferencing
- listening to podcasts

<iframe width="560" height="315" src="https://www.youtube.com/embed/RlWJR17NuRY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## The pros

- Way cheaper than their successor [Jabra Elite Active 75t](https://www.jabra.co.uk/bluetooth-headsets/jabra-elite-active-75t).
- Sound upgrade comparing to (old generation) of [Backbeat FIT](https://www.plantronics.com/sg/en/product/backbeat-fit). Please note that the Backbeats were very old at the time of the swap, so the comparison might not be fair.
- Can connect to multiple BT devices at the same time.
- Even after very heavy usage, I had to charge the box only every other day.
- If you return them to the case after use, it's virtually impossible that they will run out of battery.
- Automatically turns on after removing them from the case.
- Automatically starts playing (music) once connects to a device.
- Automatically pauses (music) once you remove one of the earbuds from your ear.

## The cons

- It's not easy to tap precisely on volume-up / volume down without knocking the headphones out from the ear.
- While it can connect to multiple devices at the same time, Apple usually hogs the connection, so you need to disconnect it before using on another device.
- Maybe this would be a pro for some people, but the isolation capabilities leave a weird sensation of "no sound" in your head. Did not like it.
- Still just micro-USB connector. No USB-C.
- After two months using those I tried very old AirPods and oh my, the Apple headphones were so much more comfortable. There is no way of putting the _65t's_ in your ears securely without a little bit of discomfort.
- If you smile a lot, they will fall off. Muscles/skin movement during deep smile is just too much for them.
- Mechanical lock on the case prevents opening with one hand and feels like it will break soon.
- I never felt secure wearing _65t's_ during sport. To be fair, they fell off from my ear only when I was not moving, never during running/cycling. But the feeling is still there and losing hundreds of pounds to insecure-by-design is not nice.
- If you wear buff or hat, the likelihood that you knock them off is high.
- The Jabra phone app feels very dated. _Find my Jabra_ function often reported each of the headphones being in different places.
- Cleaning the case from biological material is kinda a nightmare.

## Conclusion

My piece of _Jabra Elite Active 65t_ got faulty after ~2 months of usage. One or both of the buds volume started being extremely low for random periods. Trying to return them now and getting different brand/model.

For sportsting what I'm looking into now is:
 - **Wireless**: Can't be bothered with the wire nonsense ever again.
 - **100% secure fit**: During training or competition the last thing I want to worry about is headphones shenanigans.
 - **Comfort**: Probably all in-ear models will have some, even slightest level of discomfort. Try to bear with that during ultras.
