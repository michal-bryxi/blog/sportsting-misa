---
title: Loch Ness 360° as a three day hike
image: /images/2021-05-17-loch-ness-360-cover.jpg
imageMeta:
  attribution:
  attributionLink:
featured: true
authors:
  - michal-bryxi
date: Sun May 17 2021 22:00:00 GMT+0100 (British Summer Time)
tags:
  - 4-stars
  - hiking
---

Loch Ness 360° refers to a loop that joins [Great Glen Way](https://www.highland.gov.uk/greatglenway/) 
and [The South Loch Ness Trail](https://www.visitinvernesslochness.com/blog/everything-you-need-to-know-about-the-south-loch-ness-trail)
into one epic loop around the famous Lake Ness.

There are plenty of ways to break down the route. 
From commonly used and chilled [six day itinerary](https://lochness360.com/about/), 
through challenging [3 Marathons & 3 Days](https://lochness360.com/lochnesschallenge/3-marathons-3-days/) 
up to (quite bonkers) [LochNess360° Challenge](https://lochness360.com/lochnesschallenge/).

We decided to do the middle one. 
Sort of. 
We did it in three days but via hiking.

![Loch Ness 360° map](/images/2021-05-17-loch-ness-360-map.jpg)

## Logistics

For the sake of giving ourselves less and less load over time, 
we decided to break the trip unevenly: 52km, 42km, 35km. 
Which meant a bigger load on the first day, 
but more chill towards the end.

We wanted to go lightweight, 
but unfortunately for the first night everything was booked, 
so we needed to take also the camping gear. 
For food it was just trail mix and sandwiches with late lunch stops on the way. 
Water was provided by plentiful creeks + water filter.

Although weather in this area can be pretty unpredictable
we were very lucky to get a lot of sunshine and minimum winds.
Overall bogginess of the whole route is practically zero
because most of the hike can be done on very nicely maintained dirt paths.

## Day 1

Getting on the trail from the sea level of Inverness takes a bit
so we started with quite a significant altitude gain.
Beautiful old forest with mossy stone walls lined the first kilometres of the path.
Exchanged with the stunning views of the Loch Ness itself.
Meadows with cute lambs included (limited to seasonal availability).

![Forests above Inverness](/images/2021-05-17-loch-ness-360-day1-hike.jpg)

The first lunch stop was not planned and happened at Abriachan **Eco-Campsite & Cafe**
which lured us in with their wild & hippie "billboards" on the way.
If you're into fancy dining,
give it a pass.
If you're into friendly staff,
unexpected stuff and an overall warm feeling,
you should visit.

Not entirely sure whether it might have been a coincidence,
but on this leg of the trip,
we had the highest amount of people stopping to have a chat with us and exchange smiles.

On **km 47** we found the perfect spot for camping.
Bench, partially windproof shelter, usable for fireplace and stunning views.
Unfortunately according to plan,
we had 5 more km to go,
so we pushed a little bit more.
Which turned out to be a good call given the fatigue the next day.

![Perfect spot for camping](/images/2021-05-17-loch-ness-360-day1-view-site.jpg)

Our final destination for the day was at an artistic attraction called **The Viewcatcher**.
Not an amazing spot to sleep at, to be honest,
but we found a spot to pitch a tent.

![The Viewcatcher](/images/2021-05-17-loch-ness-360-day1-camp-site.jpg)

### Points of Interest

- [Abriachan Cafe Campsite Wester Laide](https://goo.gl/maps/AjguzFuw6Ek6P1VP6)
- [Perfect camping spot at km 47](https://goo.gl/maps/bXWLGrZ55Yp8rw1N8)
- [Troll bridge](https://goo.gl/maps/YpfdDKwiMkdqLJyL7)
- [The Viewcatcher](https://goo.gl/maps/xBSsGXEEG6zZu18w9)

## Day 2

Wake up. Pack all the stuff. Realize how sore the feet are. Start walking.
The first attempt to get some breakfast at **Invermoriston** was not a success (Sunday)
so we continued to **Fort Augustus**.
We opted for a first stop that advertised some _vegan_ options,
which happened to be **Caledonian Canal Centre**.
And I can't explain why (probably the tiredness),
but their bean burger & fries were from another planet 👨🏻‍🍳👌🏻.

![Loch Ness view from Fort Augustus](/images/2021-05-17-loch-ness-360-day2-fort-augustus.jpg)

Since Fort Augustus is the lowest point of the whole route,
the next part was mostly about hiking uphill.
The first few kilometres was an idyllic stroll through the meadows full of sheep & lambs.

After the highest point (436 m) the most "meh" part of the route started.
We had to walk on paths winding down the former forest that is completely dead now.
After that was several km of a straight path on the tarmac,
followed by a slightly muddy walk through farmlands.
The highlight of this part was the sighting of a parcel of a deer
that showed us their gracious skill of fence jumping and disappeared in the sunset.
Other than that I would happily skip.

Late-night arrival to **Foyers**,
a quick snap of the local waterfall
and quickly get keys from our lovely accommodation at **Foyers Roost**
which deserves an extra shout for providing us with a hot meal way past kitchen closing time.

![Foyers waterfall](/images/2021-05-17-loch-ness-360-day2-waterfall.jpg)

#### Points of Interest

- [Caledonian Canal Centre](https://g.page/CaledonianCanalCentre?share)
- [Foyers waterfall](https://goo.gl/maps/ScDg4stAyL5vxstD7)
- [Foyers Roost](https://goo.gl/maps/vuyu5epjVFRbU56k8)

## Day 3

Waking up was way easier this time.
Clean, fed and relatively happy to keep on walking for another day
we departed from the Foyers and hiked up the first big hill.
After that, we got the view of Loch Ness again.
It is surprising that despite its name most of the hike the lake is not visible at all.

![Loch Ness views](/images/2021-05-17-loch-ness-360-day3-loch-ness.jpg)

Hiking through the deep forests here on steep zig zags is very relaxing.
Occasional jumps over fallen timber is a welcome addition to the set of daily activities.
A little bit of rest in **Dores** to get a picture from the other side of the lake
and final push to get back to Inverness.

![Loch Ness view from Dores](/images/2021-05-17-loch-ness-360-day3-dores.jpg)

## Conclusion

- Subjectively the nicest part of the hike is the first one **from Inverness to Drumnadrochit**. The woods above Inverness are just gorgeous.
- If you want to avoid something it's the leg **from Fort Augustus to Foyers**. The first bit is cute as you're going through the sheep lands, but it quite quickly turns into wastelands of former forests and convoluted turns between farmlands.
- Talk to the people on the route. Everyone has a story and it's an amazing part of the hike to exchange those.
- If you can, take a plastic bag with you to do some [plogging](https://en.wikipedia.org/wiki/Plogging).
- Hiking the whole route in three days is entirely possible, but won't be easy. Prepare to have some doubts about the organizer's sanity.

## Stats

- [Relive](https://www.relive.cc/view/vr637Lz8j86)
- [Photo album](https://photos.app.goo.gl/9upuuxFtsbaPgJ8SA)
- [The Loch Ness 360 on walkhighlands.co.uk](https://www.walkhighlands.co.uk/lochness/loch-ness-360.shtml)

| Day | Distance [km] | Altitude gain [m] | Net moving time [hh:mm] |
| ---- | -------: | -----------------: | ----------------: |
| [Day 1](https://www.strava.com/activities/5303460954) |  52.2 | 1,055 |      10:44 |
| [Day 2](https://www.strava.com/activities/5309266638) |  42.5 |   949 |       9:24 |
| [Day 3](https://www.strava.com/activities/5313255369) |  35.0 |   527 |       7:32 |
| Sum   | 129.5 | 2,531 |      27:41 |

![Loch Ness 360 3 day hike route](/images/2021-05-17-loch-ness-360-profile.png)
