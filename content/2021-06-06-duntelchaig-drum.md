---
title: Duntelchaig - Drum
image: /images/2021-06-06-duntelchaig-drum-01.jpg
imageMeta:
  attribution:
  attributionLink:
featured: true
authors:
  - michal-bryxi
date: Sat Jun 26 2021 18:59:08 GMT+0100 (British Summer Time)
tags:
  - 3-stars
  - climbing
---

I never climbed a trad multi-pitch before. And one has to start somewhere, right? So why not at the most annoying multi-pitch trad climb ever?

The drive to the parking spot is just _20-something_ minutes from _Inverness_. The approach then goes for a bit on a nice path that winds around _Loch Duntelchaig_, just to turn into a horrid path-less fight through dense forest and swampy meadows. It does not take long, but I can't imagine anyone enjoying that.

After that, you arrive at the crag and have to identify the correct route. Which is not fun as there are no major, or easy to recognise features.

After a bit, my climbing partner points at a birch tree next to a waterfall and says: "This is it". Because I have no idea how to tell whether that's correct my willingness to gear up and start climbing is as enthusiastic as when someone has to go for a root canal.

![Duntelchaig - Drum; Wet start of the path](/images/2021-06-06-duntelchaig-drum-02.jpg)

After the **first pitch**, which is wet and full of greenery, we built an anchor at another birch tree. Then we swapped positions and I try to pretend that I know what to do with the clingy clangys.

**Second pitch** is just one short traverse that finishes with a massive ledge for a very comfortable anchor building.

Followed by **third pitch** which is pretty much just a walk on a slab that finishes with quite a bold two moves through a chimney.

![Duntelchaig - Drum; Views from the top](/images/2021-06-06-duntelchaig-drum-03.jpg)

The views from the top are beautiful and a break for a snack and a picture is very much recommended.

The walk-off is as annoying as the approach and the first few hundred meters are spiced up by omnipresent heather. 

## Notes

- I would very much recommend taking _all your stuff up the wall_, because you don't want to fight with nature to retrieve it.
- If you can, take the road from the East going through _Newton of Leys_. It will take you through some stunning views and the cute _Dunlichity Church_.

- [Parking](https://goo.gl/maps/hNhVAz6ePsN3eAQ36) - 57.35899318136625, -4.249761575975558
- [UKC profile for Duntelchaig](https://www.ukclimbing.com/logbook/crags/duntelchaig-388)
- 165 routes - half boulders, half trad
- Gneiss rock
- West facing
