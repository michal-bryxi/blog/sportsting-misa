---
title: Petzl Bindi battery life
image: /images/2023-05-11-petzl-bindi-battery-life-cover.jpg
imageMeta:
  attribution:
  attributionLink:
featured: true
authors:
  - michal-bryxi
date: Thu May 11 2023 14:07:49 GMT+0200 (Central European Summer Time)
tags:
---

Good friend of mine recently replaced her [Petzl Bindi][1] headtorch,
because the battery gave up
and was not able to provide required time of illumination.

So I decided to let mine undergo an empirical test.

## Manufacturer specification
- Brightness: 200 lumens (ANSI/PLATO FL 1)
- Weight: 35 g
- Beam pattern: Flood
- Energy: 680 mAh rechargeable battery
- Certification(s): CE, UKCA
- Watertightness: IPX4 (weather-resistant)

| Lighting Color | Lighting Levels | Brightness |         Distance | Burn Time | Reserve Lighting |
| -------------- | --------------- | ----------:| ----------------:| ---------:| ----------------:|
| White          | MAX BURN TIME   |       6 lm |              6 m |      50 h |                - |
|                | STANDARD        |     100 lm |             23 m |       3 h |            1.5 h |
|                | MAX POWER       |     200 lm |             36 m |       2 h |            1.5 h |
| Red            | Continuous      |       1 lm |            2.5 m |      33 h |                - |
|                | Strobe          |            | Visible at 400 m |     200 h |                - |

## My measurements

For practical, real-life limitations I did not check all the manufacturer claimed parameters.
My headtorch was bought on **2022-05-19**,
so at the time of writting this it celebrated pretty much exactly **one year**.

| Lighting Color | Lighting Levels |     Burn Time |
| -------------- | --------------- | -------------:|
| White          | MAX BURN TIME   | ~ 82 h 00 min |
|                | STANDARD        |    3 h 00 min |
|                | MAX POWER       |    2 h 20 min |

_Disclaimer_: The headtorch was _not_ used extensively during the year.
I needed to use it during few runs, climbs and cave explorations, but that was it.

## Conclusion

I am absolutely in love with this headtorch.
It weights nothing.
Can be charged over USB (albeit older micro-USB).
Has an **incredible** battery life on the lowest setting.
Does not have to be ashamed when it comes to light beam strength when needed.
Can automatically lower the brightness when the battery is running low.

Even after one year of usage the battery life is still very good.
Not sure how to explain that I measured _more_ than what Petzl shows on their website,
but that's good news from my perspective.
All and all very much a worth it purchase.

[1]: https://www.petzl.com/INT/en/Sport/Headlamps/BINDI
