---
title: Salomon Mamores VK
image: /images/2019-09-20-salomon-mamores-vk-cover.jpg
imageMeta:
  attribution:
  attributionLink:
featured: true
authors: 
  - michal-bryxi
date: Fri Sep 20 2019 17:53:33 GMT+0100 (BST)
tags:
  - 4-stars
  - running
  - race
---

A vertical kilometre is a very niche type of sport for a very special kind of people.

When I was "shopping" for competitions at the beginning of 2019 season, 
I entered the [Salomon Ring of Steall Skyrace](http://www.skylinescotland.com/ring-of-steall-skyrace/) 
and I noticed that the day before there will be also a VK. 
I thought it would be a fun idea to join both of them. 
Turned out it was.

The idea of Vertical k is pretty simple: 
The **distance** does not matter, 
only the **altitude gain**.

So they are usually on ridiculously steep hills, 
stairs, black ski slopes and similar. 
Because this one starts from the city centre of [Kinlochleven](https://goo.gl/maps/QZEhFqK7aTtBJTvh8), 
it is slightly longer than usual. 
So the gradient was "only" **20.44%**. 
On the other hand, 
at least we had a chance to warm up before the actual climb started. 
Despite the unusual dry & sunny weather the extreme bogginess of the midsection made the "run" a entertaining challenge.

> If you decide to enter two races during one weekend in Scotland, 
> make sure to bring one pair of dry shoes for each course.

Slightly surprising was [mandatory equipment](http://www.skylinescotland.com/mamores-vk/information/#displayMandatoryClothingEquipment) (fell/mountain running shoes, waterproof top with hood, waterproof trousers/pants, hat, gloves/glove-mittens suitable for the weather conditions and whistle) 
which was checked at the start of the race.

## Stats

- Location: [UK - Scotland - Peebles](https://goo.gl/maps/QZEhFqK7aTtBJTvh8)
- Price: 29£
- Distance: 4.98km
- Altitude gain: 1018m
- Time: 1:17:31
- Position: 17 of 41
- Category position: 94 of 199
- Loss on the winner: 0:32:48
- [Photo album](https://photos.app.goo.gl/iVfrrmbQbRcHgpAQA)
- [Strava](https://www.strava.com/activities/2724984368/overview)
- [Relive](https://www.relive.cc/view/vXvLYYApp1O)

![Strava profile](/images/2019-09-20-salomon-mamores-vk-01.png)

## Verdict

### Pros

- It is a very nice warmup before the [longer races](http://www.skylinescotland.com/ring-of-steall-skyrace/) if you want to run also those.
- The bog climbing experience is great fun.
- Overall the weekend in Kinlochleven is great fun.
- VK is surprisingly not a quick race.

### Cons

- I managed to get too big breakfast, so my running was mostly a fight with my stomach.
