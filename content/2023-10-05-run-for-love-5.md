---
title: Run For Love 5
image: /images/2023-10-05-run-for-love-cover.jpg
imageMeta:
  attribution: Leo Francis Photography
  attributionLink: https://instagram.com/leofrancisphotography?igshid=MzRlODBiNWFlZA==
featured: true
authors:
  - michal-bryxi
date: Thu Oct 05 2023 07:27:49 GMT+0200 (GMT+02:00)
tags:
---

First times are always special. First kiss, first tattoo, first multi-stage race. They don't have to be good or bad, they are just special. And exactly that was my first multi-stage event called [Run For Love](https://triberunforlove.com/).

## Why
According to the official website:

> TRIBE Freedom Foundation was established by the TRIBE community in 2017. Our mission is to fight modern slavery and end human trafficking. Inspired by Run for Love – an epic 1,000-mile journey to fight human trafficking, thousands of everyday athletes and adventurers have pushed their limits to raise over £1.2 million for TRIBE Freedom Foundation, enabling us to support over 10,000 survivors and potential victims across the UK. We work by raising awareness of trafficking and supporting frontline projects to fight modern slavery. We empower the TRIBE community to create an impact.
> -- [triberunforlove.com/faqs](https://triberunforlove.com/faqs)

## What
In continuation of stealing from the official website:

> TRIBE Run for Love 5 is an epic multi-day running adventure, following the spectacular Swedish High Coast Trail that stretches through the unique Höga Kusten UNESCO World Heritage Site. It is unbelievably beautiful and will be the most stunning Run for Love location to date, without question.
> -- [triberunforlove.com/faqs](https://triberunforlove.com/faqs)

- Place: Swedish High Coast
- Dates: 17 – 23 September 2023
- Distance: 225 km
- Duration: 5 days
- Average daily distance: 45 km/day

## Personal report
### Day -1
Arrival in Stockholm. Check-in at the [Generator hostel](https://staygenerator.com/hostels/stockholm?lang=en-GB). Trying to find a group of organisers & runners somewhere in the bar area. Bumping into an innocently looking group. Shaking hands with someone who introduces himself as [Tom](https://tribefreedomfoundation.com/how-we-work/) and after a wee small talk finding out that he's a TRIBE Co-Founder. Dinner. More small talk with other organisers, runners, volunteers and support crew. Afterparty at a pub. Back at the hostel. Sleep.

### Day 0
I wake up and move with my stuff to the hostel lobby where many other participants are already waiting. They are easy to recognise by almost unified ~70l duffel bags. After a while, the group starts marching towards the bus station. During the chats, I realised that I will have 48 new names to remember.

Upon arrival at the bus station, someone points at the departure sign and starts laughing. The electronic board lists a bus with a destination as "TRIBE". When the bus arrives, we load the backpacks, ourselves and begin the 6-hour journey.

Arrival at [Sweden´s first wind-powered hotel](https://www.hotellhoga-kusten.se/en). Cheeky swim in the sea. Briefing. Dinner with awesome vegan options. Repacking and sharing gear. Sleep.

### Day 1
- Distance: 54 km
- Altitude gain: 1'600 m
- Start: 06:30

![Bridge next to our hotel.](/images/2023-10-05-run-for-love-day1-01.jpg)

We wake up in a mysterious, misty mood. Breakfast. The hotel lobby is full of people who look very positive and energetic, yet some of them are nervously walking around, trying to remember what they forgot to put into their running vests and might need during the run. Group photo in front of huge inflatable TRIBE gate, line-up for the start, countdown and off we go.

I'm intentionally starting pretty much at the end. The idea is that I want to slowly progress from group to group to group and have some small talks with other runners as I go. Before my arrival, I did not know anyone.

After a few hundred meters the Högakustenbron bridge appears from the fog above our heads. One kilometre more and we're running over another bridge having the fog all around. Despite the creepy atmosphere everyone is smiling and the group is consuming the initial kilometres quickly.

The first climb of the day and one of the most magical views of the whole trip opens up when the early morning sun flares are cutting through the mist between the trees of the pine forest.

![Three people running through the forest. The picture is taken from the drone. Sun flares are going in between the trees.](/images/2023-10-05-run-for-love-day1-02.jpg)

The aid stations are plus minus every 10 km and one can very easily spot them from the distance by volunteers enthusiastically cheering at us as we are approaching. The stations are equipped with various goodies varying from water, oranges, bananas, crisps, blueberries and tons of other yummy-looking things that I never tried as I did not want to be an ass and to ask for every single thing if it's vegan. The stations also oftentimes had someone from the human-repair team ready to provide their expertise.

Soon I caught up with Hannah. A road-running, kilometres-gulping chatter-box who somehow becomes my running buddy for pretty much the rest of the trip.

After a while, we climb at the highest peak of the day and so we are above the fog and are presented with gorgeous views on the top of the clouds & the lakes around us.

During the descends on a technical terrain, we're progressing slowly and with care. Nobody wants to bust their ankle on day 1. But what we lose on that type of terrain, we certainly compensate on the tarmac. The day is quite long, but even at the end Hannah has still all the steam for her engine and because I don't want to fall behind, I'm trying to keep up as much as possible.

GPX navigation in our watches leaves very little room for mistakes and even when we take a wrong turn, the maniacal beeping from the wrist computer tells us quickly that we have to turn back and take the other route.

On the last few kilometres, Hannah's watch shows one cool feature that mine lacks: A countdown till the end of the route. Irrespective of how bad the GPX vs. GPS turns out to be and how much we ran extra, her watch just shows precisely the distance to the finish line. And that's quite cool.

Running through the finish line. Group selfie. Yay, made it through the first day. Swim in the lake. Shower. Food. Prepare to sleep in the tent. Visit the ache-fix station. All while trying to cheer at those who are continuously arriving. Quite tired after the long day, but overall I arrived in a better condition than expected.

- Elapsed time: [7h 43min](https://www.strava.com/activities/9873031576)

![Me and Hannah running through the finish gate](/images/2023-10-05-run-for-love-day1-03.jpg)

At the finish line met a girl who had an accident and accidentally headbutted Sweden. Sweden won and she had to be fast-forwarded to the finish line. She was still a bit in shock, but relatively cheerful given the situation.

During the whole day, I did not take out my speedy sticks. So I decided that I would not take them for any of the subsequent days. Compared to the training in the Alps, this terrain is practically flat.

Social chats and quite an early bed.

### Day 2
- Distance: 46 km
- Altitude gain: 1'500 m
- Start: 07:30

The start is not as early as yesterday. But since we're camping and we have one ultra behind us, everything takes longer. As tired people start peeking out from their tents, the entire village eventually wakes up.

Toilet, breakfast, teeth, pack, prepare running vest, dress up. A morning routine that will be very familiar to us for the next few days.

The weather looks grim. My watch shows a storm alert and there is forecasted rain for most of the time we're going to run. Morale is still high, but nobody is excited about the idea of going onto the trails with the prospect of getting constantly and being completely drenched for the whole time. But we're all here for a reason and despite the wet situation, nobody wants to drop the goal for the day.

I'm again intentionally starting in the back and as the kilometres go moving my position slowly up in the virtual leaderboard. Eventually, I'm catching up with Hannah's group and their pace seems just perfect for me. Narrow forest paths, grassy hills, and rocky technical bits are changing at a regular pace and omnipresent mushrooms are offering some variety for the day.

About halfway through the route the line on the map starts going through dodgy parts of the forest. We're trying to somehow follow, but the path won't be anywhere nearby. Kinda used to this from the navigation efforts during geocaching, so I'm just heading forward where the map shows me in the hope that the path will eventually re-appear. It did. We walked for another couple hundred meters and the map showed that we had to cross a river. Sadly the map did not show us a way to any bridge nearby. But our shoes were completely drenched anyway, so we did not care about this small route detour.

![People crossing a river.](/images/2023-10-05-run-for-love-day2-01.jpg)

On the next bits Hannah and I felt better than the rest of the group and so we decided to leg it. Entering an area of boulder fields and wooden paths as we're climbing up to the highest point of the route which had a tiny loop on the very top. The idea there was to show us around and give us some kick-ass views. The fog was so thick that we nearly missed a TV tower that was placed on the top of the same hill.

![Me and Hannah running through the forest.](/images/2023-10-05-run-for-love-day2-02.jpg)

Finished the loop and went to the final stretch. Few technical bits are protected with ropes. More boulder fields. Beach crossing. More wooden paths. Completely useless bridge in the middle of nowhere on a boulder field. And we're approaching the finish line.

![Me and Hannah running through the forest.](/images/2023-10-05-run-for-love-day2-03.jpg)

Final 500m of runnable beach, arriving through the gate while everyone that has already arrived is cheering at us. We've made it!

- Elapsed time: [7h 45min](https://www.strava.com/activities/9879585374)

Shower. Rehydrate food. Get warm. Visit the tape&pain station. I cheer at everyone who arrives after me and see the unbreakable human spirit as the girl, that had the mishap yesterday, arrives through the finish line. Badass.

A surprise sauna at the beach is a welcome addition. Evening bonfire at the beach. Briefing for tomorrow. An unexpected Northern Lights just before bed.

Somehow my tent feels too full to sleep in it, so I opt for sleeping on a porch where the backpacks were initially stored. At least my snoring won't wake up anyone.

### Day 3
- Distance: 31 km
- Altitude gain: 600 m
- Start: 08:00

Waking up gets harder every morning. The body does not want to move. There is something that aches above my knees. But at least we're starting into a dry day.

Beautiful start on the beach facing the sunrise. After a tiny bit of trails, we're getting onto the tarmac and I'm slowly trying to progress between the groups of people forward and forward. Eventually, I get into the zone and I can just zoom on this course. Super easy terrain allows for quite decent speeds.

The last 6km goes to the forest and a horrible, horrible bog. My progress gets very slow for this bit. For the last 1km, we're back on the tarmac and suddenly I can hear someone shouting behind me. I turn and stop to see what's up. The guy realised that I stopped because of him and gestured for me to go. Ok then.

Last 500m and I can hear music behind me. A runner with a boom box. No, I won't let anyone in front. I'm pushing all I can, but he just appears next to me like in the "12 tasks for Asterix" and effortlessly hop-by-hop reaches the finish line in front of me while I'm using my sprint speed to comically fall behind.

- Elapsed time: [3h 48min](https://www.strava.com/activities/9885570341)

![Finisher beer. Me holding bottle of Staropramen against Swedish lake.](/images/2023-10-05-run-for-love-day3-01.jpg)

Cheeky non-alcoholic beer, dinner, thinking, second dinner, and even more snacks. Sauna and cold swim in the lake. The body aches solidly this time. The station for contemplating bad life decisions is full (as always) and despite my downhill muscles being solidly overworked I'm not given any tape or drugs.

The [final 100 km](https://triberunforlove.com/the-final-100-km/) crew arrives and we have some time to have a wee chat with them. One of the newcomers is [Emily](https://www.ellas.org.uk/meet-the-team), the Co-Director of [Ella's](https://www.ellas.org.uk/) charity. The organisation describes itself as:

> We do everything we can to ensure survivors of trafficking and exploitation have all they need to recover and build lives that are safe and free.

Dawn comes, everyone is sitting around the bonfire and Emily reads a [letter from a survivor](https://youtu.be/I74jguffzJo?si=u_OWGN93t3qBaVyt&t=203).

Tomorrow is a big day. Can't stay up too long. My secret plan to sleep in the sauna was prevented by late-night sauna visitors, so instead I'm using the porch again and also apparently freaking out some of the newcomers.

### Day 4
- Distance: 60 km
- Altitude gain: 2'300 m
- Start: 05:00

When I open my eyes the village is already buzzing with life. Where are you taking the energy from people? The hope for a dryer forecast is not going to happen today and we're being greeted with light rain.

When I meet Hannah it's obvious that one of the omnipresent, bloodsucking inhabitants of North Sweden visited her at night. One of her eyelids is completely swollen and she asks me if I can stick to her for the day to help if needed. Sure thing. Starting to rain, dark, wet and with one eye? Kudos.

Start. Right after the first few hundred meters, we're presented with the reality of the next 60 km - wet shoes. The deep night part of the run lasts only for an hour or so and we soon can put the head-torches away.

At one point our group of two adopts Leti, one from the "last 100kms" crew and we have more stuff to talk about.

My downhill muscles are completely dead and I dread any parts that are not going flat or uphill. My style downhill could be at best described as "clown with diarrhoea running down the stairs".

I'm not the only one suffering. Everyone is. On surprisingly many occasions we're meeting one runner. Sometimes he is faster. Sometimes we're faster. He is completely focused on going forward, but he is in a different place in his mind. A very, very dark place. But at least it stopped raining.

Thanks to my previous training for [Harder Run](https://www.jungfrau-marathon.ch/de/harderrun.html) (750m altitude gain over 3km) I got the karma of being a mountain goat. So during the uphills, I would just dash forward and then wait for the group at the top.

On the tallest climb of the day, the route turns up and follows a set of fixed ropes and ladders on a very steep incline. A little bit of mountain goating and I'm at the top. Resting while waiting for the group to arrive. Descend with the group. What we gained in altitude we immediately lost. I'm slowly plopping behind the group as my quadriceps are killing me.

![Drone view looking down on forest, wooden path and few runners.](/images/2023-10-05-run-for-love-day4-01.jpg)

The last pit stop was at the corner of the lake where the crew prepared a bonfire and interested people could toast some marshmallows to go. And from here we're ready for the final climb of the day. Only about 12 km to go, but the body is tired from the accumulated kilometres and the long days before.

After a bit of climbing on some more technical parts, we arrive at a rocky plateau. Sadly the views are shy and so they are hiding behind a fog of clouds. But navigation is quite simple as we have to stick to the ridge and follow the blue dots that are painted on the rocks.

Curvy paths with hard, technical terrain seem to be infinite and last three kilometres we're counting down every 100m. We're trying to keep running as much as possible, but at this point, with this much fog in our heads, it's not wise to try to go fast.

Getting out of the forest. Seeing the familiar banners saying "TRIBE". Hearing the cheering of those who are already at the campsite. Final push. Going through the gate. Very emotional group hug from our team. We made it.

- Elapsed time: [11h 22min](https://www.strava.com/activities/9893072543)

This campsite does not have showers so we have to swim in the river and make wet wipes hygiene. Food. All the food disappears. The body needs it. Trying to keep warm.

Those of us who are already fed, "clean" and after the pain station are tirelessly creating a human tunnel as the last runners keep arriving. We see some extremely emotional arrivals where people burst into tears when they cross the finish line. Some very cute ones where we can see the comradery of the entire team and how they helped each other to finish. And some extremely broken people that went to the very edge of their physical capabilities.

The most challenging day is behind us, but the battle is not over.

### Day 5
- Distance: 36 km
- Altitude gain: 800 m
- Start: 08:00

Optimism, hope, outlook. All those emotions can be felt through the waking up camp as limping ghoul-like creatures are appearing from the tents. "It's not far, it's not a big day, I can do it." These are the sentences that are probably in the heads of those people. Or maybe just in mine.

Hannah, Leti and I team up right from the start. The first 10km are mostly on paths/tarmac, The legs are surprisingly fresh after yesterday so we're flying through. Leti has a slower start and sends me and Hannah ahead, which makes my companion happy as she can realise herself in her discipline of road running. I'm trying hard to not fall behind.

![Me and Hannah running on tarmac towards the camera](/images/2023-10-05-run-for-love-day5-01.jpg)

After the first pit stop path goes up through the forest on some challenging terrain, so we slow down and suddenly we can hear a group catching up with us, this group brought Leti back, and so the trio is reunited.

Kilometres don't go fast. We walk a lot. We pain a lot. But we keep going. After all those days volunteers at the pit stops still keep cheering at us and giving us smiles. Some brave ones even give hugs. This is better motivation than anything else.

![Me and Leti going through the forest.](/images/2023-10-05-run-for-love-day5-02.jpg)

Time passes in uncountable quantities, but one leg goes in front of the other like clockwork. One, two, one, two, one... Suddenly we're over the last hill and we can see the city. With my eyes, I'm scanning the concrete jungle to find a familiar blue-yellow pattern. My tired memory serves me information about the hotel on the coast, so I narrowed my attention to that part of the city. "There!", I scream ecstatically, "The gate is next to the tall, grey building!". The team morale suddenly jumps four levels up. We're at the sight of the finish line.

Quick navigation through the maze of the city. Unexpected detour around and behind the hotel. Nervous waiting at the zebra crossing for the green person. And we're "sprinting" through the finish line.

- Elapsed time: [5h 15min](https://www.strava.com/activities/9899301541)

![Me, Hannah and Leti at the finish line.](/images/2023-10-05-run-for-love-day5-03.jpg)

Team hug. Laughter. Big emotions. Happiness. Everyone who already finished gets out of the hotel restaurant and gives us big applause and hugs. We did it!

A quick search for the bags. Check-in at the room. Shower. Order beer. And very voluntary "get out and cheer" with every new group arriving. Many people are in tears as they cross the finish line. The atmosphere is phenomenal. The afterparty does not last that long, but nobody complains as everyone is excited to sleep in a real bed.

## Finishing thoughts

What an incredible, unforgettable, awesome experience it was.

Kudos to everyone participating in the race. Hat down to the event organisers - RatRace. Bow to the team putting this all together - TRIBE. Big thanks to the media team geniuses - Leo & James. And the biggest of hugs to all the volunteers, medics and physios.

## Lessons learned
- Yes, I can run a multi-stage ultra. 4 marathon+ distances are achievable at a reasonable pace.
- Running in a team is surprisingly faster than solo. I would never, ever manage to keep running on the flats at such a speed if Hannah would not keep pushing.
- Physios are miraculous people with magic wands embedded in their fingers. Not exaggerating if I say that half of us would not have finished if they had not been present.
- Due to my recent training in the Alps, I'm a bit better with the uphills than the other runners.
- Running downhill requires some weird muscles under the kneecap and those can be overworked to the point of a terrible pain.
- To not cramp/cramp less my body needs 500ml of ionts for 500ml of pure water.
- People are awesome.

## Media

### The Challenge Commences

<iframe width="560" height="315" src="https://www.youtube.com/embed/E-IbSB6snVE?si=tIlbdANDGDsXhceb" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

### Braving the Storm

<iframe width="560" height="315" src="https://www.youtube.com/embed/jpcWqxyUPio?si=myd3YK2WH-HINSZC" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

### The Longest Day

<iframe width="560" height="315" src="https://www.youtube.com/embed/o1clgJxJbwQ?si=UU3WX8y-u5f34eyD" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

### An unforgettable journey

<iframe width="560" height="315" src="https://www.youtube.com/embed/I74jguffzJo?si=tXLTWZXzTZdbLL1P" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## Links
- [James Appleton Photography - Videos - Instagram](https://www.instagram.com/jamesappletonphotography/?hl=de)
- [Leo Francis Photography - Photos - Instagram](https://instagram.com/leofrancisphotography?igshid=MzRlODBiNWFlZA==) - good pictures used in this post are his work
- [We Are Tribe - Videos - YouTube](https://www.youtube.com/@wearetribe1835)
- [RatRace - Event organiser](https://www.ratrace.com/)
