---
title: Thieves Road Race 2019
image: /images/2019-08-24-thieves-road-race-cover.jpg
imageMeta:
  attribution:
  attributionLink:
featured: true
authors: 
  - michal-bryxi
date: Sun Aug 25 2019 09:23:03 GMT+0100 (BST)
tags:
  - 2-stars
  - running
  - race
---

Report from my first proper ultra.

This brand new *40 miler* was originally planned for a date I could not join, but due to unusual stormy weather forecast, it got postponed by two weeks. Which was exactly what I needed to put my name on the list. [Described](https://runyabam.com/race-information/thievesroad/) as:

> Suitable for all, no ultra-marathon completion needed.

with cutoffs being advertised as: 

> Runners should be capable of an average speed of 4mph.

I had no fear that it would be a *DNF*.

Day before the race I went for a lunch with my friend Lesley-Anne  who nudged me into joining. Two things that she brought to my attention: Because of a need to re-plan the first bit, the route got *3 extra miles*. And even though the terrain is [described](https://runyabam.com/race-information/thievesroad/) as: 

> Drove roads, forest trails, some road between sections.

the route is actually going to be pretty **serious trail**. Suddenly my road shoes that are usually "good enough" were a "no-no".

The centre-point of the whole race is [Edderston farm](https://goo.gl/maps/XtH922TCcuPTxgVA9) in Peebles. This is where the registration happens, where you have your drop bag and where the race finish.

![Thieves Road Race 2019 course map](/images/2019-08-24-thieves-road-race-map.jpg)

## The day before

Unfortunately because the registration ends at *8:00am* it is **not** possible to take morning bus from Edinburgh to Peebles. So I bought a return ticket for *10£* on the bus [X62](https://www.bordersbuses.co.uk/services/X62), and pitched a tent for *12£* in [Crossburn caravans](https://www.crossburn-caravans.com).

## Before the race

Since this was the *first* year of *Thieves Road Race*, the number of participants was quite low: *60*. And since this type of races tend to tempt always the same kind of people, everyone knew everyone. Except for me & Lesley.

Registration time. Say hello to organizers, pick up your bib, prepare drop bag, fill my water bottles, load running vest with "food", take starting selfie, get to the bus and off we go to the start. Took us nearly an hour to get to the start, so this long day started at *09:21am*.

## The race

![Thieves Road Race 2019 elevation profile](/images/2019-08-24-thieves-road-race-graph.jpg)

First **3km** was on a nice dusty road. Once we started gaining some altitude, the terrain changed to being seriously boggy & wet, with next to zero chance of keeping your shoes dry.

Lesley and I ran **5km** together. We were chatting and a laughing on the absurd terrain. After that I head on, on my own pace.

From **kilometer 10** on the terrain got much nicer with really pretty trails and occasional parts on the road.

**Kilometer 17** and we're at the first water station where we were also supposed to dib the [dibber](https://www.sportident.co.uk/equipment/shop/category.php?id=1). As advertised no food here, so I came prepared with *4 energy gels* planning to have one per *10km*. *1l* of water refilled in my bottles, and in couple of minutes I went on to see the next kilometers.

The route was marked with yellow signs on trees or orange arrows on the ground. Since there was very few of us running, most of the time I had to navigate by myself. The marking was there only when I had to make a turn. Nearly zero "you are still on the right track" marks. Which was fine, most of the time, unless it wasn't. At one X-shaped crossroads I met a sign with an arrow clearly going "to the left". The way to left was to a top of a hill I just ran around. Right was downhill. And straight continued around the hill, while slowly turning left. I felt like going up the hill was wrong, so I consulted the route with my watch and I got a confirmation that I should continue straight.

Thanks to the hills and unusually warm weather (up to **24℃**) I ran out of water as early as **km 30**. At the entrance to Peebles (*km 35*), there was an awesome opportunity to place another water station. Was not there, hopefully next year? It was only 5km back to the farm to get anything I wanted, but running *5km* with completely dry throat was not pleasant. During the course I decided to stop at every river I passed by to wash my face. Helped a lot.

Roughly **40km** in and I arrived back to the farm, there was also the bag drop. I drank 1l of water right on the spot, got 1l of fresh water to my bottles, changed my socks, put more fuel in the running vest and off I go towards the hilly part.

After another kilometer of running, I spotted bright orange pieces of fabric on the trees leading me up a hill into a forest. Started going up and after a while, I double-checked my watch and found out that I'm definitelly not on the right track. So I started descending, trying to reconnect to my route. I picked up my phone and called the organisers, describing what happened asking them to warn other runners. They confirmed that orange is not the color I should follow.

**Kilometer 43** and I'm meeting another water station. The position of this one still puzzles me. Everyone had to have completely full bottles at this point. Even putting this little further into the hills on **km 45**, where the route going back re-joins, would be so much more useful. So I got my little bit refueled and went on.

Next *20km* between **km 46** and **km 66** were seriously a character building. Some quotes from other participants: "F\*cking brutal", "Relentless climbs", "Adventure", "F&cking adventure", "Bloody awful". Nearly **550m** altitude gain after running a marathon distance is nothing pleasant. Add **1/4** of the route in bog so horrible that losing a shoe was a very much a possible option. A cherry on top was that **1/2** of the route was on undergrowth so big that I often felt it touching my groin area. First bits climbing up and I met a guy going down. I thought he was the leading runner, so I cheered at him. But he told me that he has cramps so horrible that he is giving up. 

Half way up the steepest climb I met a guy hopelessly lying on his back in the mud. I stopped to check on him, he got up and we walked together for a bit. He had plenty of food and water, but his internal motor was completely f&cked. After a kilometer or so when I saw that he's probably not going to die, I headed on. Next thing I'm going to jump a fence, but just before that I notice that next to the usual arrow, there is the dibber beeping machine. I wonder how many people managed to miss this? My water supplies dissapeared even before reaching the top of the mountain. Obligatory picture from the top, turn right, and start descending.

Marking is so sporadic that it was only an intuition and my watch that kept me "on course". Not even kilometer into the descent and I met a guy coming from completely illogical direction. He got lost. At that point I was so glad for the ability to load a *gpx route* into my watch. Him, me and one other guy started a horrible descend that promised a sprained ankle at every other step. When we finally arrived on something that one could call a "path", the other guys headed on. I was seriously dehydrated at that point and my stomach started playing games, so from now on the only running for me was during descents.

At **km 66**, there would be another perfect place to put a water station - remember the re-joining point? Unfortunatelly this did not happen and even the lady with the illogically placed water station dissapeared. So I was counting meters to get back "home" and get a sip of any liquid.

As I was approaching the finish line the sun was already really low and I remembered joking with Lesley that "I don't plan to get back after sunset, so headtorch is not necessary". I was not wrong, but restrospectively it was a very silly decision.

A little bit of getting lost on the last bits. See the finish line at the bottom of the hill. Enthusiastic cheering from the crowd of organisers and a phony attempt of run through the finish line.

Get medal, sit down, drink, drink more, eat something savoury, get inside the cottage, drink more tea, change clothes and finally realise what just happened.

## Stats

- Location: [UK - Scotland - Peebles](https://goo.gl/maps/PzCZTYbym2fDngAq8)
- Price: 42£
- Distance: 69.7km
- Altitude gain: 1690m
- Time: 10:06:42
- Position: 17 of 41
- Category position: 4 of 9
- Loss on the winner: 2:23:58
- Pace: 08:42min/km
- [Photo album](https://photos.app.goo.gl/SYag7819YM22XT4JA)
- [Strava](https://www.strava.com/activities/2648084820/overview)

## Verdict

### Pros

- This was my longest running race ever.
- I don't feel completely destroyed the next day, looks like I'm getting better at pasing myself?
- A lot of character building, especcialy on the parts with more challenging terrain.
- Very friendly community.

### Cons

- Sometimes **marking** would really need some more thought. In some places I had really doubts whether "this way" or "that way".
- The wording about **terrain** on the website was kidna misleading. Doing this as my first ultra and having less of a willpower, I would most probably give up at some point. Although the sweepers were very friendly and supportive, I would not recommend this course to beginners.
- Water was my Achilles the whole route. I would recommend taking **1.5l** of water **at minimum**. Again the fixtures said "means of carrying water", which does not really give an impression of how much one should take.

![My selfie with group of runners in the background. Lesley vawing my way.](/images/2019-08-24-thieves-road-race-01.jpg)
![My watch showing the furtherst distance I've ever run till now - 50km.](/images/2019-08-24-thieves-road-race-02.jpg)
![Beautiful view on a field with stone wall & a gate.](/images/2019-08-24-thieves-road-race-03.jpg)
![Sheeps running away from me.](/images/2019-08-24-thieves-road-race-04.jpg)
