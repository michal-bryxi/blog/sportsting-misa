---
title: Ben Chonzie
image: /images/2021-06-21-ben-chonzie-01.jpg
imageMeta:
  attribution:
  attributionLink:
featured: true
authors:
  - michal-bryxi
date: Mon Jun 21 2021 21:59:26 GMT+0100 (British Summer Time)
tags: 
  - 2-stars
  - munro
---

For [Hogmanay](https://en.wikipedia.org/wiki/Hogmanay) 2020 we tried to summit **Ben Chonzie** from the **Loch Turret reservoir**. Unfortunately, we underestimated what seemed like a mild winter and had to bail off 2km from the summit because of knee-deep snow and horrible winds.

## Second attempt

The second attempt took place on a scorching hot (for Scotland) day. _Group A_ decided that it will take the direct route from [Ben Chonzie Car Park](https://goo.gl/maps/WNwHkthU7eYSXd1w6), from the side of **Glen Lednock** taking **12.5km** forth and back. While _Group B_ decided to take a jog through **Carn Chòis**.

The first part of the trip goes through beautiful meadows full of cows, jumps over a river, winds between a couple of ponds and ends with zig zags climbing up the top of _Carn Chòis_.

The second part was mostly flat, but on somewhat more severe terrain that was extremely boggy even though the days were really dry. The final stretch on a rocky path and both groups met at the top of _Ben Chonzie_ which is overlooking the beautiful _Loch Turret_.

![View from the top of Ben Chonzie](/images/2021-06-21-ben-chonzie-02.jpg)

The last bit on the way down was common for both teams and consisted of very solid dust paths, that was occasionally full of stones making descent relatively slow.

![Ben Chonzie from Glen Lednock](/images/2021-06-21-ben-chonzie-03.jpg)

## Conclusion

Overall I think the detour up that _Team B_ took was way nicer and should not be missed if possible. The views there are well worth it.

## Stats

![Strava of Ben Chonzie](/images/2021-06-21-ben-chonzie-04.png)

- [Strava activity](https://www.strava.com/activities/5384371175)
- [Ben Chonzie on walkhighlands.co.uk](https://www.walkhighlands.co.uk/perthshire/ben-chonzie.shtml)
- **Distance:** 24.98km
- **Jogging time:** 3h 44min
