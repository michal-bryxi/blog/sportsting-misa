---
title: Scott Kinabalu Ultra Rc review
image: /images/2024-05-30-scott-kinabalu-ultra-rc-cover.jpg
imageMeta:
  attribution: "Dall-E: Image of a sad runner because their old, used, dirty running shoes are falling apart --ar 16:9"
  attributionLink:
featured: true
authors:
  - michal-bryxi
date: Thu May 30 2024 08:27:13 GMT+0200 (Central European Summer Time)
tags: 
  - review
  - gear
  - shoes
---

Official webpage of [SCOTT for Kinabalu Ultra RC](https://www.scott-sports.com/us/en/kinabalu-ultra-rc) says:

> The SCOTT Kinabalu Ultra RC is our go-to shoe for ultras and long training miles away from the big mountains. A combination of our Hybrid Traction outsole, which delivers straight-line power transfer on man-made trails, and the responsive cushioning from our Kinetic Foam midsole returning 14% more energy, plus the rock plate for protection create the perfect shoe for trail ultra marathons.

I got mine on an offer from [bergzeit](https://www.bergzeit.ch/search/?q=SCOTT+Kinabalu+Ultra+RC&ms=true) for _CHF 95.20_, which seemed like a great deal.

Sadly I have to report that this was the pair of shoes with the **shortest lifespan** I ever owned. Overall I took them for:

- 50% of [Run For Love 5](../2023-10-05-run-for-love-5)
- Few local trail runs in the Alps

That's it. The shoes clocked pretty much **exactly 345.4km** (thanks [Strava](https://www.strava.com/settings/gear)). They are really destroyed from the upper mesh part, which developed the usual "squat holes", my specific "toe hole" and other minor wounds. And parts of the under-grippy-sole, has pretty much transformed to a completely flat surface with next to zero grip.

All and all this is _way less_ than what I'd expect from running shoes. So if you see them for a very good price ... you'll get what you pay for.

## Pictures

![Scott Kinabalu Ultra RC](/images/2024-05-30-scott-kinabalu-ultra-rc-01.jpg)
![Scott Kinabalu Ultra RC](/images/2024-05-30-scott-kinabalu-ultra-rc-02.jpg)
