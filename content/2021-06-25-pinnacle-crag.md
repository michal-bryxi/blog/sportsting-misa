---
title: Pinnacle Crag
image: /images/2021-06-25-pinnacle-crag-01.jpg
imageMeta:
  attribution:
  attributionLink:
featured: true
authors:
  - michal-bryxi
date: Fri Jun 25 2021 22:39:23 GMT+0100 (British Summer Time)
tags:
  - 4-stars
  - climbing
---

Closest **trad climbing** area from _Inverness_. Very short access. No gardening required.

> Small crag. Only 10m high but with 15 routes. Nice looking bit of rock. Great for trad beginners in the Inverness-shire area.
> -UKC-

Lead my first Very Difficult trad route here called [Tapered Groove](https://www.ukclimbing.com/logbook/crags/pinnacle_crag-2705/tapered_groove-44308). Beautiful views on nearby **Loch Duntelchaig** available from the top. Pleasant rock with a lot of features, comfortable belaying spots and easy down-climb walk-off.

![Pinnalce Crag](/images/2021-06-25-pinnacle-crag-02.jpg)

## Notes

- [UKC profile for Pinnacle Crag](https://www.ukclimbing.com/logbook/crags/pinnacle_crag-2705)
- [Parking](https://goo.gl/maps/1HLLMYGVWcuoYwXH8): 57.358964922420135, -4.2497006185767265
- 24 routes, trad only
- Gneiss rock
- South facing
