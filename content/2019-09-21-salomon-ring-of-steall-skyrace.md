---
title: Salomon Ring of Steall Skyrace
image: /images/2019-09-21-salomon-ring-of-steall-skyrace-cover.jpg
imageMeta:
  attribution:
  attributionLink:
featured: true
authors: 
  - michal-bryxi
date: Sat Sep 21 2019 17:53:33 GMT+0100 (BST)
tags:
  - 5-stars
  - running
  - race
---

An epic adventure in the Scottish Mamores.

For a long time [Spelga Skyline](http://barfni.co.uk/events/spelga-skyline/) was the top running competition when it came to toughness and physical challenge for me. 
But I think I can finally say that _Spelga met it's equal_.

## Basic information

The plan was simple:
Rent a car, 
arrive at the [Kinlochleven](https://goo.gl/maps/QZEhFqK7aTtBJTvh8) couple of days earlier,
enjoy the beautiful, wild, Scottish nature,
warm-up on Friday at [Mamores VK](/2019-09-20-salomon-mamores-vk),
and enjoy the [Ring of Steall](http://www.skylinescotland.com/ring-of-steall-skyrace/) on Saturday.

Most of the "roof included" accommodations in the extended area are hopelessly sold out a long time before the race. 
So the easiest (and let's admit it - cheapest) option for us was to go directly for one of the [recommended camping places](http://www.skylinescotland.com/location-overview/).

The whole [event village](http://www.skylinescotland.com/location-overview/) is very well equipped. Plenty of parking spaces, free showers, film night, [Ice Factor](https://www.ice-factor.co.uk/) for chillout and after-party.

> [Skyrunning](https://en.wikipedia.org/wiki/Skyrunning) is an extreme sport of mountain running above 2,000 metres (6,600 ft) 
> where the incline exceeds 30% 
> and the climbing difficulty does not exceed II grade.

### Mandatory kit

There is quite a long list of [mandatory kit](http://www.skylinescotland.com/ring-of-steall-skyrace/information/#displayMandatoryClothingEquipment) that **will** *all* be **checked** just before the race start, no matter the beautiful, sunny Scottish weather.

## The race

The route consists of two big climbs.
At the end of the first downclimb,
there is an *aid station* 
(see the pace drop on the graph below). 
After that, you will transfer with the mountains on your right. 
And then another set of climbs
(the pace drops here are totally only because I was enjoying the scenery 😬).
For those who got hooked onto [bagging the Munros](https://www.munromap.co.uk),
the good news is that you will get **four** of them on the way.
Admittedly although it's a very effective way to collect Munros,
it's definitely not the wisest one.


### First half

Because I'm horrible with reading manuals,
I did not have *the map* during the kit check
and I needed to find it somewhere in the mess of our hobo car.
And because of that delay
I was starting somewhere far back. 
So ** the first kilometre** through Kinlochleven was just me trying to get in front of the crowds.

After that, we joined the [West Highland Way](https://www.westhighlandway.org/)
and followed it till approximately **km 3**
when we made a sharp turn right
and started properly gaining altitude.
The slightly muddy terrain was fun to navigate through.
Hopping from stone to stone,
trying to not get my shoes wet.
At one point the course took a detour through true Scottish MOG
(a combination of beautiful *meadow* and horrendous *bog*)
and then I realised
that caring about such a temporary thing
as dry shoes during such race were silly,
so I went all in and just enjoyed the fun parts.

**6 km** into the race and we're taking a sharp left.
This is where the mud turned into solid ground
and a little bit of scree.
Also, it's the point where we will close the circle and go down.

We got to the first "peak" at **km 6.5**.
The route after that were reasonable bits up and down on a nice narrow ridge.
Not too narrow to not able able to get in front of people,
but not too wide to make it boring.

As I was traversing the ridge,
I saw a guy "swimming" against the stream of runners.
I knew that face from somewhere.
"Is that Scott",
I shouted in the voice of a child opening a Christmas present.
It was him.
The guy that helped me the most during [Running the Blades]() and suddenly disappeared
was out of a blue here in the middle of Mamroes running the wrong way.
When I asked him about it,
he just pointed at his knee and mumbled something about an injury.
He was here to support his brother who was competing.
We gave each other high-five and continued each our own way.
What a legend.

![First ridge](/images/2019-09-21-salomon-ring-of-steall-skyrace-03.jpg)

First *Munro* awaits us at **km 8**
and my watch shows **1092 m** above sea level.
Kinlochleven is at **15 m**,
so it was more than a *vertical km* already.

Except for the steep climbs,
and "mountain queuing simulator" during the first 5km or so,
I was running most of the parts,
so retrospectively *pat, pat, pat*
for upping my mountain goating game.

Top of **Sgùrr a Mhàim**,
take a quick look at spectacular **Ben Nevis** in the distance
and begin descending.

First few steps on the omnipresent scree
and the person in front of me nearly finished on their bum.
No sign of slowing down though.
Few people got in front of me as I was trying to figure out
how to deal with the terrain.
As we got into more and more rapid descents,
the scree got less and less stable.
So eventually I just copied the style
of the girl right in front of me that I internally called **the scree surfing™️**.
It consisted of small jumps here and there
interleaved with a lot of ... well ... scree surfing.
Did not take long and I was somewhat confident with my new skill
(famous last words, eh?).
So I started gaining speed while going down the zig-zags
and getting in front of some of the runners.
At the same time, some other runners just zipped next to me like nothing.
I had an amazing time,
when suddenly I can hear someone shouting "stone, stooone!".
I looked down in the direction where people pointed at
and there was this boulder the size of a laptop backpack rolling down the hill.
I tried to help by also shouting at the people further down the hill.
Some of them tried to take cover by going sideways,
but one guy was weirdly frozen in place.
He just stood there,
visibly exhausted,
facing down the hill (as one should),
just hopelessly having his hands behind his head
which would have the protective effect
like two paper umbrellas from a Mojito would have against
light Scottish "drizzle".
The boulder rolled less than 5 meters from him
and disappeared somewhere off the course.
Everybody was visibly relieved,
but the race continued in pretty much-unchanged pace.

Descending lower and lower my thighs started getting weak.
Running become harder and harder.
Scree surfing turned into scree power hiking
and that turned into desperate scree braking.
The last couple of hundred of meters of altitude was quite rough to descend.
Finally some flat.
Opening the cow gate.
Going through.
Legs are weak.
For the first time a significant number of runners passed by me.
I would say they were just hungrier than me.

### Pit stop

First and the only aid station was at **km 11.3**.
First refill of water (*1l*) for me.
Several oranges are the only thing that my gel irritated stomach would agree with.
Shake the stones out from my shoes,
quickly go to the loo,
and off we go for the second half.

### Second half

My running becomes really heavy.
Although till **km 14.5** the course was mostly flat,
I was merely pretending a race.
It was a bit of a paradox,
but at this point, my power hiking became way more efficient for the purpose of racing.

Next kilometre or so we were literally climbing up the former river.
Stopping at a waterfall to replenish water
and wash the sweat from our faces.

After that descend and crossing of the *Water of Nevis*.
My body is starting to overheat.
So I wished that the walk in the river would be longer.

And at **km 16.3** off we go climbing to the next Munro the **An Gearanach**.
The whole group of runners become one gigantic *slug* slowly rolling uphill,
with occasional *ant* that sprinted forward like crazy.
I'm not the only one suffering.
I'm passing quite a lot of runners that are sitting next to the zig-zags,
with their heads in their heads.
I'm offering gummi bears,
everyone politely refuses and
cheers me up as I'm passing by.

I'm ditching any idea of another gel.
Maybe the gummi bear things would work?
Chewing one of those always kicked the morale back up for a few minutes.
But the fight was harder and harder with each step.

**Km 19.1** and finally the top of *the second Munro*.
Right at the top is a guy on all four and he can't stop puking.
His friend is standing next to him
and she is having the most sincere "I'm sorry for you" face.
Glad that I did not take the risk with the gels.
Enjoy the view on the beautiful ridge
and get back to proper running.
If you want to call it running.
In the distance, I can hear *bagpipes*, 
so we can't be far from the finish line now.

Little up and down and we're at **km 20.3** at the top of *the third Munro* **Scob Coire a' Chairn**,
where we meet the source of bagpiping.
Group of fans hiked up here,
to stay in the blazing wing for hours
to cheer runner by playing one bagpipe tune on repeat.
Oh, I love this country.

Next ridge and someone is telling me:
"Only two more hills to go".
"But that is one more than it should be",
I'm telling myself in the head.
Confused and exhausted I'm starting thinking about **DNF**.
"Well, I'm not going to run one extra freaking mountain",
says one voice in my head.
"Like you have a magic teleporter to get your ass from the mountains anyway",
says the other.
Sane voice won
and I kept moving.
Till now I can't tell which one was the sane one.

After that, I met Scott again.
His brother was way in front of me.
He offered me salt tablets, I refused.
As he continued running towards the stream of the runners,
he turned back once again and shouted:
"Maybe I'm your guardian angel".
I have not met him in the event village after that.

![Trying to pretend that I can still run](/images/2019-09-21-salomon-ring-of-steall-skyrace-01.jpg)

The last ridge was awesome.
It got sharper, rockier.
Terrain way more technical.
Running turns into careful jumping
and that turns into careful walking.
Eventually, we are traversing on a rock face with the ridge on our right.
A lot of places require hand-feet coordination
as we're scrambling forward.
At one point I put my right foot forward too quickly and too much to the left
and that forces me to put my left foot somewhere forward.
But there is no space on the left side of my now.
Only the steep slope of rocks.
So I counterbalance by putting my left foot far right,
and gracefully land on my hands and bum on the rock on my right.
Phew, that was close.
Hard to pay attention while so desperately tired.
I try to continue as safely as possible,
while not obstructing the path to the people behind me.

Finally, at **km 21.8** we meet the last *Munro* - **Am Bodach**.
At this point, we already got our **~2400 m** of climbs.
My gummi bears are gone.

![Last hill and some of us changed it to sunbathing competition](/images/2019-09-21-salomon-ring-of-steall-skyrace-05.jpg)

![My selfie with hopelessly lying person behind me](/images/2019-09-21-salomon-ring-of-steall-skyrace-04.jpg)

Slow descent to **km 22.6** and we're rejoining the trail that took us here.
Former slightly muddy terrain became a mud trap.
Hundreds of runners going up the hill
and several dozens who already descended changed the face of the trail significantly.
Trap next to a trap.
Shoes that dried while running on the top of the hills were completely soaked in a matter of seconds.
Running here looked like running through landmine field.
You are a few steps behind a person,
trying to keep the pace with them,
when suddenly they disappear from your view.
One of their legs ended up to their knee in a mud pit.
So you choose a slightly different route
and the run continues with you leading through the bog mine-field.

Another stop for water.
I absolutely could care less that slightly higher up the stream a frog was having a bath.
And quickly back on the course.
My descend was very slow compared to all the other runners who overtook me.

Approximately at **km 26.3** we got back on the solid dusty path with occasional steps from stones.
For whatever reason, I felt like this was a treat.
So I started properly running again.
Chasing some slightly faster people down the hill,
and enjoying the technical terrain was really beautiful.

**Km 27.8** and we're back on the tarmac in Kinlochleven.
Last stretch.
My body is screaming.
I'm walking.
Last turn.
I can see the crowds on the bridge.
I'm running again.
Kora is cheering at me and making me smile to the camera.
I'm trying to not give up.
A lot of "f words" would be on the camera.
Final "sprint" and after **28.55 km** and **6 hours 34 minutes** the **Ring of Steall 2019** is done for me.

Get the medal.
Hand over the chip and live GPS device.
Final photo.
And collapse on the curb next to heaps of other runners.
What an adventure.

## Stats

- Location: [UK - Scotland - Kinlochleven](https://goo.gl/maps/QZEhFqK7aTtBJTvh8)
- Price: 99£
- Distance: 28.5km
- Altitude gain: 2468m
- Time: 6:34:24
- Position: 302 of 690
- Category position: 178 of 331
- Loss on the winner: 3:14:47
- [Photo album](https://photos.app.goo.gl/iVfrrmbQbRcHgpAQA)
- [Strava](https://www.strava.com/activities/2728138351/overview)
- [Relive](https://www.relive.cc/view/vxOQAAYmX2O)

![Strava profile](/images/2019-09-21-salomon-ring-of-steall-skyrace-02.png)

## Verdict

### Pros
 - Skyraces are an amazing adventure.
 - The first climb, ridge and descent are the best running experience I had so far.
 - Spectacular views if you're lucky enough to get decent weather.
 - Friendly people trying to help each other.
 - A lot of altitude gain in one race.
 - Ridge running is so much fun.
 - Live GPX tracking.
 - The event village is full of very friendly people & extra activities.
 - During the weekend, there is a lot of races of various distances.
 - Organisers are really serious about mandatory equipment.

### Cons
 - Slightly more expensive.
 - The buffet after the race should be way bigger.
 - One aid station means you should carry a lot of food.
 - The weather in this area is mostly shite.

## Extras


- [Ring of Steall Skyrace 💪💥 Golden Trail World Series 🏴󠁧󠁢󠁳󠁣󠁴󠁿🏆](https://www.facebook.com/RingOfSteallSkyrace/videos/521861648567096/)
- [Day 2 at Salomon Skyline Scotland 2019](https://www.facebook.com/RingOfSteallSkyrace/videos/447323952554420/)
- [2019 Salomon Skyline Scotland Highlights](https://www.facebook.com/RingOfSteallSkyrace/videos/549092692494254/)

How does the local hillwalkers see *Ring of Steall Skyrace* (6:08):

<iframe width="560" height="315" src="https://www.youtube.com/embed/-h4c8Z95I3E?start=368" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

> It's possible to do all the eleven Munros in one day if you're both super fit and extremely stupid.
