---
title: Tromsø skyrace - Tromsdalstinden
image: /images/2019-08-03-tromso-skyrace-tromsdalstind-cover.jpg
imageMeta:
  attribution:
  attributionLink:
featured: true
authors: 
  - michal-bryxi
date: Sun Aug 03 2019 09:23:03 GMT+0100 (BST)
tags:
  - 4-stars
  - running
  - race
---

Amazing views & heavy legs.

Everyone is talking about the beautiful landscapes around Norway and some are whole-heartedly whispering about the (sky)races there. So I thought it would be a good idea to try it on my own. And it was.

Getting to and from Tromsø is definitelly the most financially challenging part of this trip. The flight tickets from UK were pretty much the same price as I would pay for my South-East Asia trips. Accomodation on the other hand was fine. [4 bed dormitory](https://www.airbnb.co.uk/rooms/28179792?source_impression_id=p3_1567329218_ccTxdR9QU8jo4UEI) in a very high standard BnB for [18.5£ per night](https://www.airbnb.co.uk/rooms/28179792?source_impression_id=p3_1567412733_6qIv8Azn9y5D5Bcr) is something I was pleasantly surprised with. The bonus of staying at shared accomodation is making friends on the way. There was a mother with two young(er) daughters, an uni student, extremely tall Polish guy and an awesome Scottish guy living in Oslo. Except the mother - all runners. There was also a mid-age Norwegian guy - constantly out partying or drunk.

## Before the race

Nordic eternal day did not allow me to sleep properly, but the day of the race I did not feel sleepy. In fact during the four day visit, I did not feel tired at all.

Morning, registration, prepare food and water, mandatory selfie with my new friends and off we go.

![Me and my random friends at the start of the race](/images/2019-08-03-tromso-skyrace-tromsdalstind-02.jpg)

## The race

Start is at the sea level. First **3 km** were quick, flat tarmac. We crossed the bridge from the island part and started hiking up the first hill. **5.5 km** in and we're at the first water station at *Storsteinen* (**421 m**). Next high point is at **km 9** the summit of *Bønntuva* (**769 m**). Followed by quick descent back to **504 m** where we needed to cross the river. Volunteers over there warned us that this is the last place where we can refill our water supplies. Apparently thanks to lack of fauna in the area it's perfectly safe to drink from any creek.

Till here I was pretty fine and was running most of the time (except for extremely steep climbs). **km 11.5** to **km 17** - from the river to summit of *Tromsdalstinden* (**1238 m**) showed me that I was not really prepared for the altitude gains in this race. Power hike turned into hike and that turned into a desperate slow-walk. The higher we got the bigger stones we had to walk on. The bigger the stones, the steeper climb it was. At one point I felt like I'm crawling at all fours. Legs were shaking and tempo was ridiculously slow. But to be fair to myself: till here I was pretty much keeping the same position in the group.

Summit of *Tromsdalstinden*, quick selfie, admire the person that just scrambled up the snow patch on the other side of the mountain - he was running the *Hamperokken Skyrace* (57 km). And start descending. The boulders we had to jump from/to were really big at this point. Descend really steep and a lot of unstable soil. Thanks to my Elvis legs I was not worried, because my progress was extremely slow. I can see on my [Strava flyby](https://labs.strava.com/flyby/viewer/#2587033578?c=ukms005f&z=C&t=1THLSC) that I started falling behind. At one point one lady passed by me so fast like the rough terrain was non existent. She was way faster on the descent than anyone else I met.

![Obligatory selfie from the summit of Tromsdalstinden](/images/2019-08-03-tromso-skyrace-tromsdalstind-03.jpg)
![Picture from the top of the mountain with a person in the back who just scrambled up the snow patch](/images/2019-08-03-tromso-skyrace-tromsdalstind-04.jpg)

At **km 20** we finally meet another river. It was only **8.5 km** since the last water source, but my water levels (*1 l* in sum) were pretty low.

Next part was pretty much flat, very nice running. But my energy levels were so low, that I'm struggling to even keep half-and-half of running/walking ratio. The only socially enforced run is happening at **km 24** and here is why: I was told about this before. Apparently [Kílian](https://en.wikipedia.org/wiki/Kílian_Jornet_Burgada) loves to joke about this part during the briefing. At this point you can se Tromsø right in front of you. You can see the beautiful dusty path getting there. But instead of going back to Tromsø the route goes "nope" and turns sharp left back to the hills. There were several volunteers making sure exhausted runners know where to go. And of course I did not want to just walk past them, so a little bit of running happened here.

Another **184 m** of altitude gain and we're back on the summit of *Storsteinen*. From here (**km 27**), it's only going downhill. My energy levels are deeply in red numbers and I have to descend **420 m** of altitude. After getting back on tarmac we had just *3 km* to go. Very supportive volunteers on every corner showed us the way. Cross the bridge, run next to the marinas, pretend to speed up for the last kilometer or so and finally sweet, sweet finish line.

![Tromsø skyrace - Tromsdalstind race map](/images/2019-08-03-tromso-skyrace-tromsdalstind-map.jpg)
![Tromsø skyrace - Tromsdalstind elevation profile](/images/2019-08-03-tromso-skyrace-tromsdalstind-graph.png)

## Stats

- [Official website](https://tromsoskyrace.com/)
- Location: [Norway - Tromsø](https://goo.gl/maps/T6s1X6vdTHm5pBQt8)
- Price: 73£
- Distance: 32km
- Altitude gain: 1739m
- Time: 5:27:46
- Position: 281 of 422
- Category position: 219 of 300
- Loss on the winner: 2:29:44
- Pace: 10:15min/km
- [Photo album](https://photos.app.goo.gl/rm1gYDrNx9mRv2p39)
- [Strava](https://www.strava.com/activities/2587033578)

## Verdict

### Pros

- Amazing views of wild Norwegian nature. Needless to say that fog/rain is normal thing in this area, so we were extremely lucky to get such a good weather.
- Some ascents and the descent from Tromsdalstinden is very technical.
- A lot of opportunities to do activities around before/after the race.
- Plentiful water re-supply opportunities from the rivers around.
- Even though most of the route is not marked, there is no chance to get lost.

### Cons

- It is not a cheap trip to do.
- Till the summit of Tromsdalstinden it feels mostly like climbing stairs. 2 hours 46 minutes of climbing stairs in my case.

### Personal

- My decision to get a gel every *10km* was not the best. I certainly needed way more energy input than that.
- Carb loading is a thing. Definitely have to change dinner the day before.
- I was so surprised to learn that the I would be able to pass the first cutoff time for Hamperokken (3 hours at the summit of Tromsdalstind). My time was *2hours 46minutes*.

### Official video

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/bJy2_NRCVZ0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
