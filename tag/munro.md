---
name: Munro
image:
imageMeta:
---

Munros are defined as Scottish mountains over 3,000 feet (914.4 m) in height. The original list of Munros dates from 1891, and after some changes over the years currently counts [282 Munros](https://www.munromap.co.uk/).
