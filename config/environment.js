'use strict';

module.exports = function (environment) {
  let ENV = {
    modulePrefix: 'sportsting-misa',
    environment,
    rootURL: '/',
    locationType: 'auto',

    metricsAdapters: [
      {
        name: 'Mixpanel',
        environments: ['production'],
        config: {
          token: '474ac5717e7c292a429f5d56ed3ab593',
        },
      },
    ],

    showdown: {
      tables: true,
    },

    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. EMBER_NATIVE_DECORATOR_SUPPORT: true
      },
      EXTEND_PROTOTYPES: {
        // Prevent Ember Data from overriding Date.parse.
        Date: false,
      },
    },

    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created
    },

    blog: {
      title: 'Sportsting Míša',
      description: '🪂 + ⛰️🏃‍♂️ + 🧗‍♂️ + 🚴‍♂️ + ⛷️ + 🗺️ + 🌱',
      coverImage: '/images/index.jpg',

      navigation: [
        {
          label: 'Home',
          route: 'index',
        },
        {
          label: 'Munros',
          route: 'tag',
          id: 'munro',
        },
        {
          label: 'About me',
          route: 'page',
          id: 'about-me',
        },
      ],
    },

    'responsive-image': {
      sourceDir: 'images',
      destinationDir: 'responsive-images',
      quality: 80,
      supportedWidths: [2000, 1000, 600, 300],
      removeSourceDir: false,
      justCopy: false,
      extensions: ['jpg', 'jpeg', 'png', 'gif'],
    },
  };

  if (environment === 'development') {
    // ENV.APP.LOG_RESOLVER = true;
    // ENV.APP.LOG_ACTIVE_GENERATION = true;
    // ENV.APP.LOG_TRANSITIONS = true;
    // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    // ENV.APP.LOG_VIEW_LOOKUPS = true;
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
    ENV.APP.autoboot = false;
  }

  if (environment === 'production') {
    // here you can enable a production-specific feature
    ENV.blog.host = 'https://sportsting-misa.pudr.com';
  }

  return ENV;
};
