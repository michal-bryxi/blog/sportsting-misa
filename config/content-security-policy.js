module.exports = function () {
  return {
    delivery: ['meta'],
    policy: {
      'default-src': ["'none'"],
      'script-src': [
        "'self'",
        'http://cdn.mxpnl.com/libs/mixpanel-2-latest.min.js',
        'https://cdn.mxpnl.com/libs/mixpanel-2-latest.min.js',
      ],
      'font-src': ["'self'"],
      'connect-src': ["'self'", 'https://api-js.mixpanel.com'],
      'img-src': ["'self'", 'https://s.gravatar.com/'],
      'style-src': ["'self'", "'unsafe-inline'"],
      'frame-src': ['https://www.youtube.com/embed/'],
      'media-src': null,
      'manifest-src': ["'self'"],
    },
    reportOnly: false,
  };
};
